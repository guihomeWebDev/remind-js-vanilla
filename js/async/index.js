async function test() {
    return await new Promise(
        (resolve) => {
        setTimeout(
            () => {
            resolve('hello')
        }, 1000
        )
    }
    )
}

test().then(
    (result) => {
        console.log(result)
    }
)