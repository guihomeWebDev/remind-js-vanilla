function test(mycallback, mycallbackback){
    const num = Math.round(Math.random()*10)
    mycallback()
    mycallbackback()
    number(num)
    return true
}

function mycallback(){
    console.log('hello')
}
function mycallbackback(){
    console.log('wordl')
}
function number(num){
    console.log(num)
}

test(mycallback, mycallbackback, number)