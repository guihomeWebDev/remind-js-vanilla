//create builder

class Cat {
    legs = 4;
    name = '';
}

class CatBuilder{
    constructor(){
        this.cat = new Cat;
    }
    buildName(name){
        this.cat.name = name;
        return this;
    }
    buildLegs(legs){
        this.cat.legs = legs;
        return this;
    }
    build(){
        return this.cat;
    }
}
const builder = new CatBuilder()
const cat = builder.buildName('Tom').buildLegs(4).build();
console.log(cat)