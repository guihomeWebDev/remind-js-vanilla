//cas classique
// class Tv {
//     constructor(light) {
//         this.light = light;
//     }

//     turnOn() {
//         console.log('TV is on');
//         this.light.turnOff();
//     }
//     turnOff() {
//         console.log('TV is off');
//     }
// }

// class Light {
//     turnOn() {
//         console.log('Light is on');
//     }
//     turnOff() {
//         console.log('Light is off');
//     }
// }
// const light = new Light();
// const tv = new Tv(light);
// tv.turnOn();

//cas avec facade

class Tv {
    turnOn() {
        console.log('TV is on');
    }
    turnOff() {
        console.log('TV is off');
    }
}

class Light {
    turnOn() {
        console.log('Light is on');
    }
    turnOff() {
        console.log('Light is off');
    }
}

class facade {
    constructor(Light, Tv) {
        this.light = light;
        this.tv = tv;
    }

    turnOn() {
        this.light.turnOn();
        this.tv.turnOff();
    }
    turnOff() {
        this.light.turnOff();
        this.tv.turnOn();
    }
}
const light = new Light();
const tv = new Tv(light);
const home = new facade(light, tv);
home.turnOff();