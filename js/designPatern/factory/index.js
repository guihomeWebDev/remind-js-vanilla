//create

class Animal {
    makeSong() {
        console.log('muet')
    }
}

class Cat extends Animal {
    makeSong() {
        console.log('meow')
    }
}   

class dog extends Animal {
    makeSong() {
        console.log('woof')
    }
}

function factory(type) {
    switch (type) {
        case 'cat':
            return new Cat()
        case 'dog':
            return new dog()
        default:
            return new Animal()
    }
}

const nestor = factory('cat')
nestor.makeSong()

const bob = factory('dog')
bob.makeSong()