//strategie

class Auchan{
    selCola(){
        return 150;
    }
}

class Lidl{
     selCola(){
        return 100;
    }
}

class Carrefour{
     selCola(){
        return 120;
    }
}

class Strategie{
    setStrategie(retailer){
        this.retailer = retailer;
    }
    selCola(){
        return this.retailer.selCola();
    }
}

const auchan = new Auchan();
const lidl = new Lidl();
const retailer = new Strategie();
retailer.setStrategie(auchan)
console.log(retailer.selCola())

retailer.setStrategie(lidl)
console.log(retailer.selCola())
