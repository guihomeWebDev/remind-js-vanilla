//promise
const promise = new Promise( (resolve, reject) => {
 const y = Math.round(Math.random()*10)
    if ( y < 7 ) {
        reject(y);
    } else {
        resolve(y);
    }
})

promise.then(
    (result) => {return result * 2 }
).then(
    (result) => {return result * -1 } 
).then(
    (result) => {console.log(result) }
).catch(
    () => {console.log('error') }
    )
